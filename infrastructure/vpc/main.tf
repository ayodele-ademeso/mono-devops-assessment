locals {
  prefix        = "mono-devops-assessment"
  vpc_name      = "${local.prefix}-vpc"
  vpc_cidr      = var.vpc_cidr
  common_tags   = {
    Name = "ayodele"
  }
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = local.vpc_name
  cidr = local.vpc_cidr

  azs = ["${var.aws_region}a", "${var.aws_region}b"]

  public_subnets = [
    cidrsubnet(local.vpc_cidr, 8, 0),
    cidrsubnet(local.vpc_cidr, 8, 1)
  ]

  private_subnets = [
    cidrsubnet(local.vpc_cidr, 8, 2),
    cidrsubnet(local.vpc_cidr, 8, 3)
  ]

  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true


  tags = merge(
    {
      Name = local.vpc_name
    },
    #local.common_tags,
  )

  public_subnet_tags = {
    Name =                                          "${local.prefix}-public-sg"
    "kubernetes.io/role/elb"                        = "1"
    "kubernetes.io/cluster/${local.prefix}-cluster" = "shared"
  }

  private_subnet_tags = {
    Name                                           = "${local.prefix}-private-sg"
    "kubernetes.io/cluster/${local.prefix}-cluster" = "shared"
    "kubernetes.io/role/internal-elb"              = 1
  }
}