terraform {
  backend "s3" {
    bucket = "terraform-mono-task"
    key    = "mono-task.tfstate"
    region = "us-east-1"
  }
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">= 2.49"
    }
    docker = {
      source = "kreuzwerker/docker"
      version = "2.20.2"
    }

  }
}