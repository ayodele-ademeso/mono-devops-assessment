output "registry_id" {
  value       = module.this.enabled ? aws_ecr_repository.name[local.image_names[0]].registry_id : ""
  description = "Registry ID"
}

output "name" {
  value       = module.this.enabled ? aws_ecr_repository.name[local.image_names[0]].name : ""
  description = "Name of first repository created"
}


output "repository_url" {
  value       = module.this.enabled ? aws_ecr_repository.name[local.image_names[0]].repository_url : ""
  description = "URL of first repository created"
}