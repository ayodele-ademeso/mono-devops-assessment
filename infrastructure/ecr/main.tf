resource "aws_ecr_repository" "ecr-repo" {
  name                 = var.repo_name
  image_tag_mutability = "MUTABLE"
}

resource "aws_ecr_repository_policy" "repo-policy" {
  repository = aws_ecr_repository.ecr-repo.name
  policy     = <<EOF
  {
    "Version": "2008-10-17",
    "Statement": [
      {
        "Sid": "adds full ecr access to the repository",
        "Effect": "Allow",
        "Principal": "*",
        "Action": [
          "ecr:BatchCheckLayerAvailability",
          "ecr:BatchGetImage",
          "ecr:CompleteLayerUpload",
          "ecr:GetDownloadUrlForLayer",
          "ecr:GetLifecyclePolicy",
          "ecr:InitiateLayerUpload",
          "ecr:PutImage",
          "ecr:UploadLayerPart"
        ]
      }
    ]
  }
  EOF
}


resource "null_resource" "name" {
  provisioner "remote-exec" {
    inline = [
      "sudo chmod +x ../scriptname.sh",
      "sh ../scriptname.sh"
    ]
  }
  depends_on = [aws_ecr_repository.ecr-repo]
}
